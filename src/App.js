import React, { Component } from 'react';
import './App.css';

import NavBar from './Components/navBar'
import MainContent from './Components/mainContent'
import SideBar from './Components/sideBar'


class App extends Component {
  render() {
    return (
      <div className="App">
        <NavBar/>
        <div className="sidebar-divider">
          <div className="sidebar-dimensions">
            <SideBar/>
          </div>
          <div className="maincontent-dimensions">
            <MainContent/>
          </div>
        </div>
      </div>
    );
  }
}

export default App;

import React, { Component } from 'react'
import '../../stylesheets/Dual.css';

class DualSlider extends Component{
  render(){
    return(
      <div>
        <h4>General Search Criteria</h4>
        <form>
            <div className="input-container">
                <input type="text" className="sliderValue" data-index="0" defaultValue="10"/>
                <input type="text" className="sliderValue" data-index="1" defaultValue="30"/>
            </div>
            <br />
            <div id="slider"></div>
        </form>

      </div>
    )
  }
}

export default DualSlider;

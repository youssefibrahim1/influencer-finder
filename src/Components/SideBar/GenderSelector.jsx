import React, { Component } from 'react'
import '../../App.css';


class GenderSelector extends Component{
  constructor(){
    super();
    this.state={
      selectedOption:'None',
    }
    this.handleChange=this.handleChange.bind(this)
  }

  handleChange(e){
    this.setState({
      selectedOption:e.target.value,
    })
  }

  render(){
    return(
      <div className="gender-selector-container">
        <h4>Audience Gender (last 30 days)</h4>
          <h4 className="option-state">{this.state.selectedOption}</h4>
            <div className="gender-container">
              <input id="None" type="radio" name="radio" value="None" onChange={this.handleChange} />
                <label className="select-gender none-sign" htmlFor="None"></label>

              <input id="Male" type="radio" name="radio" value="Male" onChange={this.handleChange} />
                <label className="select-gender male-sign" htmlFor="Male"></label>

              <input id="Female" type="radio" name="radio" value="Female" onChange={this.handleChange}/>
                <label className="select-gender female-sign" htmlFor="Female"></label>
            </div>
        </div>
    )
  }
}

export default GenderSelector;

import React, { Component } from 'react'
import '../../index.css';

let multiSelect = [
  'Artists','Automotive','Beauty','Business','Dads','Doctors',
  'Fashion','Fitness','Food','Gaming','Kids','Lifestyle',
  'Luxury','Moms','Musicians','Nutritionists','Photography'
  ,'PublicSpeakers','Sports','Tourism','Travel'
]


class Vertical extends Component{
    render(){
      return(
        <div className="vertical-container">
          <h5>Vertical</h5>
          <div className="scroll-container" id="scroll-customization">
            {multiSelect.map((category,i)=>{
              return(
                <div key={i}>
                  <input type="checkbox" id={i}/>
                  <label htmlFor={i}>{category}</label>
                </div>
              )
            })
          }
        </div>
      </div>
    )
  }
}

export default Vertical;

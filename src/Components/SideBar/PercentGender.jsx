import React, { Component } from 'react'
import '../../stylesheets/genderselect.css';


class PercentGender extends Component{
  constructor(){
    super();
    this.state={
      gender:0,
    }
    this.genderPercent=this.genderPercent.bind(this)
    this.textChange=this.textChange.bind(this)

  }

  genderPercent(e){
    this.setState({
      gender:e.target.value,
    })
  }

  textChange(e){
    this.setState({
      gender:e.target.value,
    })
  }

  render(){
    return(
      <div>
        <div>
          <div className="genderpercent-display">
            <div>

              <input id="gender-change-color" className="input-text" type="percent" size="4" value={this.state.gender} onChange={this.textChange}/>
            </div>
            <h4 className= "gender-text">Gender Percent</h4>
            <div>
            </div>
          </div>
          <div className="container">
            <input type="range" min="0" max="100" step="5" value={this.state.gender} onChange={this.genderPercent}/>
          </div>
        </div>
      </div>
    )
  }
}

export default PercentGender;

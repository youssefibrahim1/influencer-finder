import React, { Component } from 'react'
import '../App.css';
import AlfanLogo from '../assets/alfan-logo.png'


class NavBar extends Component{
  render(){
    return(
      <div className="App-header">
        <img alt="Company-logo" className="img-dimensions" src={AlfanLogo}/>
        <header className="header-dimensions"><h1>Influencer Finder</h1></header>
      </div>
    )
  }
}

export default NavBar;

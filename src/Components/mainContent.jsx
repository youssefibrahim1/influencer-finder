import React, { Component } from 'react'
import '../App.css';
import QualityContent from './contentDetails/qualityContent'
import Triangle from '../assets/triangle.png'


class MainContent extends Component{
  render(){
    return(
      <aside>
        <div className="change-background">
          <div className="score-container">
            <h5>Sort by: <strong>Peg Score</strong></h5>
            <img className="triangle-img" src={Triangle} alt=""/>

          </div>
          <QualityContent/>
        </div>
      </aside>
    )
  }
}

export default MainContent;

import React, { Component } from 'react'
import '../../App.css';

import Verified from '../../assets/verified-account.png'
import Unverified from '../../assets/unverified.png'

import Rubius from '../../assets/logos/elrubiuslogo.png'
import Vanoss from '../../assets/logos/vanoss.png'
import Pewds from '../../assets/logos/pewdiepie.png'
import NigaHiga from '../../assets/logos/nigahiga.png'
import Fernan from '../../assets/logos/fernan.png'
import Zoella from '../../assets/logos/zoella.png'
import Squeezie from '../../assets/logos/squeezie.png'
import Jenna from '../../assets/logos/jenna.png'
import Mark from '../../assets/logos/markiplier.png'
import Thatcher from '../../assets/logos/thatcher.png'
import Wolfie from '../../assets/logos/wolfie.png'
import Dude from '../../assets/logos/dude.png'

import Canada from '../../assets/flags/canada.png'
import France from '../../assets/flags/france.png'
import Salvador from '../../assets/flags/salvador.png'
import Spain from '../../assets/flags/spain.png'
import Sweden from '../../assets/flags/sweden.png'
import Uk from '../../assets/flags/uk.png'
import Usa from '../../assets/flags/usa.png'

let contentArray = [
  {
    pic:`${Rubius}`,
    name:'elrubiusOMG',
    country:'Spain',
    flag:`${Spain}`,
    category:'Comedy',
    avgviews:'6.5M',
    subs:'25.6M',
    verif:`${Unverified}`,
  },
  {
    pic:`${Vanoss}`,
    name:'VanossGaming',
    country:'Canada',
    flag:`${Canada}`,
    category:'Gaming',
    avgviews:'4.2M',
    subs:'21.4M',
    verif:`${Unverified}`,
  },
  {
    pic:`${Pewds}`,
    name:'PewDiePie',
    country:'Sweden',
    flag:`${Sweden}`,
    category:'Gaming',
    avgviews:'3.5M',
    subs:'57.2M',
    verif:`${Unverified}`,
  },  {
      pic:`${NigaHiga}`,
      name:'NigaHiga',
      country:'USA',
      flag:`${Usa}`,
      category:'Comedy',
      avgviews:'5.1M',
      subs:'20.1M',
      verif:`${Unverified}`,
    },
    {
      pic:`${Fernan}`,
      name:'Fernanfloo',
      country:'El Salvador',
      flag:`${Salvador}`,
      category:'Gaming',
      avgviews:'8.8M',
      subs:'23.7M',
      verif:`${Unverified}`,
    },
    {
      pic:`${Zoella}`,
      name:'Zoella',
      country:'United Kingdom',
      flag:`${Uk}`,
      category:'Style',
      avgviews:'2.1M',
      subs:'12M',
      verif:`${Verified}`,
    },  {
        pic:`${Squeezie}`,
        name:'Squeezie',
        country:'France',
        flag:`${France}`,
        category:'Gaming',
        avgviews:'3.6M',
        subs:'8.8M',
        verif:`${Unverified}`,
      },
      {
        pic:`${Jenna}`,
        name:'JennaMarbles',
        country:'USA',
        flag:`${Usa}`,
        category:'Comedy',
        avgviews:'3.4M',
        subs:'17.6M',
        verif:`${Unverified}`,
      },{
        pic:`${Mark}`,
        name:'Markiplier',
        country:'USA',
        flag:`${Usa}`,
        category:'Gaming',
        avgviews:'1.1M',
        subs:'18.4M',
        verif:`${Verified}`,
      },{
        pic:`${Thatcher}`,
        name:'ThatcherJoe',
        country:'United Kingdom',
        flag:`${Uk}`,
        category:'Comedy',
        avgviews:'2M',
        subs:'8.1M',
        verif:`${Verified}`,
      }
      ,{
        pic:`${Wolfie}`,
        name:'WolfieRaps',
        country:'Canada',
        flag:`${Canada}`,
        category:'Comedy',
        avgviews:'1.9M',
        subs:'7M',
        verif:`${Unverified}`,
      }
      ,{
        pic:`${Dude}`,
        name:'Dude Perfect',
        country:'USA',
        flag:`${Usa}`,
        category:'sports',
        avgviews:'26.8M',
        subs:'22.3M',
        verif:`${Unverified}`,
      }
]

class QualityContent extends Component{
  render(){
    return(
      <aside>
        <div className="container-1" >
          {contentArray.map((tuber,i)=>{
            return(
              <div className="bg-light border organize" key={i}>
                <img src={tuber.pic} className="img-size" alt=""/>
                <div className="text-control">
                  <span className="flag-display">
                    <h4>{tuber.name}</h4>  <img src={tuber.verif} className="verified-sign" alt=""/>
                  </span>
                  <span className="flag-display">
                    <img src={tuber.flag} className="tiny-flag" alt=""/><h6 className="flag-text">{tuber.country} | {tuber.category}</h6>
                  </span>
                  <h6>{tuber.avgviews} Avg Views | {tuber.subs} Subscribers</h6>
                </div>
              </div>
            )}
          )}
        </div>
      </aside>
    )
  }
}

export default QualityContent;

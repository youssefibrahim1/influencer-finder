import React, { Component } from 'react'
import '../App.css';

import DualSlider from './SideBar/DualSlider'
import GenderSelector from './SideBar/GenderSelector'
import PercentGender from './SideBar/PercentGender'
import Vertical from './SideBar/Vertical'





class SideBar extends Component{
  render(){
    return(
      <aside>
        <DualSlider/>
        <hr/>
        <Vertical/>
        <hr/>
        <GenderSelector/>
        <hr/>
        <PercentGender/>
      </aside>
    )
  }
}

export default SideBar;
